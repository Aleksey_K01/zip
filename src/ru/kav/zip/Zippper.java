package ru.kav.zip;


import java.io.*;
import java.util.Scanner;
import java.util.zip.*;

public class Zippper {

        public static void main(String[] args) {
            if (getEnteredString("Выберите режим работы\n1 - упаковать файлы\n2 - распаковать архив\n_> ").equals("1")) {
                File selectedDir;
                do {
                    selectedDir = new File(getEnteredString("Введите путь до дирректории с файлами: "));
                    if (!(selectedDir.exists() && selectedDir.isDirectory())) {
                        System.out.println("Введённый вами путь не найден или не является дирректорией. Повторите попытку");
                    }
                }
                while (!(selectedDir.exists() && selectedDir.isDirectory()));
                while (true) {
                    File selectedFile;
                    do {
                        printFilesListInDir(selectedDir.getAbsolutePath());
                        selectedFile = new File(selectedDir.getAbsolutePath() + '\\' + getEnteredString("Введите название выбранного файла: "));
                        if (!selectedFile.exists() || selectedFile.isDirectory()) {
                            System.out.println("Введённый вами файл не найден. Повторите попытку");
                        }
                    }
                    while (!selectedFile.exists() || selectedFile.isDirectory());
                    String archive = getEnteredString("Введите путь до архива, в который необходимо сохранить файл: ");
                    System.out.println("Упаковываем файл...");
                    packIntoZipArchive(selectedFile.getAbsolutePath(), archive);
                    System.out.println("Готово.");
                }
            } else {
                String archivePath;
                do {
                    archivePath = getEnteredString("Введите путь до архива, который нужно распаковать: ");
                } while (!new File(archivePath).exists());
                String pathToExtract = getEnteredString("Введите путь до папки, в которую будет распакован архив: ");
                unpackFromZipArchive(archivePath, pathToExtract);
            }
        }

        /**
         * Метод печатает в коммандную строку имена всех файлов, найденных в указаной дирректории
         * @param directory дирректория для чтения списка файлов
         */
        public static void printFilesListInDir(String directory) {
            File pathDirectory = new File(directory);
            File[] files = pathDirectory.listFiles();
            for (File file:files) {
                if (files != null && file.isFile()){
                    System.out.println(file.getName());
                }
            }
        }


        /**
         * Метод возвращает строку, введённую пользователем.
         * @param message Сообщение для пояснений
         * @return строка, которую ввёл пользователь
         */
        private static String getEnteredString(String message) {
            System.out.print(message);
            return new Scanner(System.in).nextLine();
        }

        /**
         * Метод создаёт zip - архив с новым файлом @code{filename}.
         * @param filename Файл, который нужно добавить в архив
         * @param outputArchive Выходной архив
         */
        private static void packIntoZipArchive(String filename, String outputArchive) {
            try (ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(outputArchive));
                 FileInputStream fis = new FileInputStream(filename)) {
                ZipEntry entry1 = new ZipEntry(filename);
                zout.putNextEntry(entry1);
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                zout.write(buffer);
                zout.closeEntry();
                zout.finish();
            }
            catch(Exception ex){
                System.out.println(ex.getMessage());
            }
        }

    /**
     * Метод распаковывает архив .
     * @param inputArchive архив,который нужно распаковать
     * @param outputDir выходная папка
     */
        private static void unpackFromZipArchive(String inputArchive, String outputDir) {
            try(ZipInputStream zin = new ZipInputStream(new FileInputStream(inputArchive))) {
                ZipEntry currentEntry;
                while ((currentEntry = zin.getNextEntry()) != null) {
                    if (!currentEntry.isDirectory()) {
                        System.out.println("Имя файла: " + currentEntry.getName());
                        try (FileOutputStream outFile = new FileOutputStream(outputDir + '\\' + new File(currentEntry.getName()))) {
                             int c;
                            while((c = zin.read()) != -1){//пока есть что читать он записывает
                                outFile.write(c);
                            }
                            outFile.flush();
                            zin.closeEntry();
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }
